import sys
import inputs

def main():
        #TODO reformat to make it print cleaner
        help = ("COMMANDS:\n"
        "  discover  Output a comprehensive, human-readable list of all discovered inputs to the system. Techniques include both crawling and guessing.\n"
        "  test      Discover all inputs, then attempt a list of exploit vectors on those inputs. Report potential vulnerabilities.\n"
        "OPTIONS:\n"
        "  --custom-auth=string     Signal that the fuzzer should use hard-coded authentication for a specific application (e.g. dvwa). Optional.\n"
        "  Discover options:\n"
        "    --common-words=file    Newline-delimited file of common words to be used in page guessing and input guessing. Required.\n"
        "  Test options:\n"
        "    --vectors=file         Newline-delimited file of common exploits to vulnerabilities. Required.\n"
        "    --sensitive=file       Newline-delimited file data that should never be leaked. It's assumed that this data is in the application\'s database (e.g. test data), but is not reported in any response. Required.\n"
        "    --random=[true|false]  When off, try each input to each page systematically.  When on, choose a random page, then a random input field and test all vectors. Default: false.\n"
        "    --slow=500             Number of milliseconds considered when a response is considered \"slow\". Default is 500 milliseconds\n"
        "Examples:\n"
        "  # Discover inputs\n"
        "  fuzz discover http://localhost:8080 --common-words=mywords.txt\n"
        "  # Discover inputs to DVWA using our hard-coded authentication\n"
        "  fuzz discover http://localhost:8080 --common-words=mywords.txt\n"
        "  # Discover and Test DVWA without randomness\n"
        "  fuzz test http://localhost:8080 --custom-auth=dvwa --common-words=words.txt --vectors=vectors.txt --sensitive=creditcards.txt --random=false\n")
        
        cmdargs = sys.argv.copy()
        if len(cmdargs) < 3: #at minimum: script name, command, url
                print(help)
                exit(0)
        cmdargs.pop(0) # remove the name of the script

        custom_auth = "" #optional
        
        command = cmdargs.pop(0) # type
        if(command == "discover"):
                url = cmdargs.pop(0)
                common_words = "" #required arg
                
                for arg in cmdargs:
                        argval = arg.split("=")
                        if(argval[0] == "--custom-auth"):
                                custom_auth = argval[1]
                        elif(argval[0] == "--common-words"):
                                common_words = argval[1]
                        else:
                                print("Unrecognized option detected, ignoring\n")
                if(common_words == ""):
                        print("Required args not present. Try help.\n")
                else:
                        discover(url, common_words, custom_auth)
        elif(command == "test"):
                url = cmdargs.pop(0)

                #default args handled here
                vectors = "" #required
                sensitive = "" #required
                random = False #optional
                slow = 500 #optional
                
                for arg in cmdargs:
                        argval = arg.split("=")
                        if(argval[0] == "--custom-auth"):
                                custom_auth = argval[1]
                        elif(argval[0] == "--vectors"):
                                vectors = argval[1]
                        elif(argval[0] == "--sensitive"):
                                sensitive = argval[1]
                        elif(argval[0] == "--random"):
                                random = bool(argval[1] == "true")
                        elif(argval[0] == "--slow"):
                                slow = int(argval[1])
                        else:
                                print("Unrecognized option detected, ignoring\n")
                if(vectors == "" or sensitive == ""):
                        print("Required args not present. Try help.")
                else:
                        test(url, vectors, sensitive, custom_auth, random, slow)

        elif(command == "help"):
                print(help)
        else:
                print("Command not recognized.\n")
                print(help)

#Discover: Run input and page discovery on the given url
def discover(url, common_words_file, custom_auth):
        inputs.run(url, common_words_file, custom_auth)

#Test: test the given page by first discovering inputs and then using predefined
#      attack vectors
def test(url, vectors, sensitive, custom_auth, random, slow):
        discover(url, "", custom_auth)
        #TODO test

if __name__ == "__main__":
        main()

