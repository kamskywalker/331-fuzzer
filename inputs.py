#!/usr/bin/env python3

import requests
from html.parser import HTMLParser
from urllib.parse import urlparse

class PageParser(HTMLParser):
    inputs = []
    links = []
    def handle_starttag(self, tag, attrs):
        if tag == 'input':
            self.inputs.append(attrs)
        if tag == 'a':
            self.links.append(attrs)
    def handle_endtag(self, tag):
        pass
    def handle_data(self, data):
        pass

class Fuzzer():
    def __init__(self, url, sensitive=None):
        self.__url = url
        r = requests.get(url)
        # Populate Values
        self.__text = r.text
        self.__cookies = r.cookies
        self.__responseTime = r.elapsed
        self.__responseCode = r.status_code
        # Parse Text for links
        self.__parser = PageParser()
        self.__parser.feed(r.text)
        self.__leaks = []
        if sensitive is not None:
            for line in open(sensitive):
                if line.strip() in r.text:
                    self.__leaks.append(line)
    def getResponseTime(self):
        return self.__responseTime
    def getResponseCode(self):
        return self.__responseCode
    def getCookies(self):
        return self.__cookies
    def getLinks(self):
        return self.__parser.links
    def getInputs(self):
        return self.__parser.inputs
    def getUrlParts(self):
        return urlparse(self.__url)
    def getLeaks(self):
        return self.__leaks

def printEntry(entry):
    for pair in entry:
        print('\t {0}: {1}'.format(pair[0], pair[1]))

#given a base url and a file of words, test each word as a subpage of the base url
def testPages(url, wordfile):
    pages = []
    for line in open(wordfile):
        a = testPage(url + "/" + line)
        if(a != None):
            pages.append(a)
        b = testPage(url + "/" + line + ".jsp")
        if(b != None):
            pages.append(b)
        c = testPage(url + "/" + line + ".html")
        if(c != None):
            pages.append(c)
        d = testPage(url + "/" + line + ".php")
        if(d != None):
            pages.append(d)
    return pages

#Test an individual url for a 200 response code
def testPage(url):
    r = requests.get(url)
    if(r.status_code == 200):
        return url

def run(url, wordsFile, auth):
    fuzzer = Fuzzer(url)

    urlParts = fuzzer.getUrlParts()
    inputs = fuzzer.getInputs()
    links = fuzzer.getLinks()
    leaks = fuzzer.getLeaks()
    cookies = fuzzer.getCookies()
    pages = testPages(url, wordsFile)

    #print results
    print("-" * 80)
    print("URL parts:")
    print(urlParts) # TODO
    
    print("-" * 80)
    print("Inputs:")
    for inp in inputs:
        print('* Input:')
        printEntry(inp)

    print("-" * 80)
    print("Links:")
    for link in links:
        print('* Link:')
        printEntry(link)

    print("-" * 80)
    print("Leaks:")
    for leak in leaks:
        print('* Leak:{0}'.format(leak))

    print("-" * 80)
    print("Cookies:")
    print(cookies)
    
    print("-" * 80)
    print("Pages from words file:")
    for page in pages:
        print(page)
