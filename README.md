# Requirements
* Python 3.4+

# Installation
Run `pip install -r requirements.txt` to install the required libraries
[requests](http://docs.python-requests.org/en/latest/).

# Running
Once installed, the fuzzer can be run via `python3 fuzzer.py ...`, where '...'
is the rest of the command

#Feedback from Craig
Include a message if no commands since it breaks if nothing is passed.