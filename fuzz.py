#!/usr/bin/env python3

"""Fuzzer.

Usage:
    fuzz.py discover <url> [--custom-auth=<ca>] --common-words=<cw_file>
    fuzz.py test <url> [--custom-auth=<ca>] --vectors=<v_file> --sensitive=<s_file> [--random=<random>] [--slow=<slow>]

Options:
    --custom-auth=<ca_file>  Which authentication to use such as dvwa or bodgeit
    --common-words=<cw_file>  Files for common words
    --vectors=<v_file>  File containing which vectors to use
    --sensitive=<s_file>  File containing sensitive data
    --random=<random>  When true, perform random testing; when false, go systematically [default: false]
    --slow=<slow>  Speed in ms at which a response is slow [default: 500]

"""

from docopt import docopt

if __name__ == "__main__":
    args = docopt(__doc__)

    if args['discover']:
        # TODO: Discover mode
        pass
    elif args['test']:
        # TODO: Test mode
        pass
